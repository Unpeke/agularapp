import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Routes
import { FeatureRoutingModule } from './app.routes';
//Services
import { HerosService } from './services/heros.services';
//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HerosComponent } from './components/heros/heros.component';
import { HeroComponent } from './components/hero/hero.component';
import { SearchHeroComponent } from './components/shared/navbar/search-hero/search-hero.component';
import { CardHeroComponent } from './components/card-hero/card-hero.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    HerosComponent,
    HeroComponent,
    SearchHeroComponent,
    CardHeroComponent,
  ],
  imports: [BrowserModule, FeatureRoutingModule],
  providers: [
    HerosService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
