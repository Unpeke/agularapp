import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HerosService, Hero} from '../../services/heros.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-card-hero',
  templateUrl: './card-hero.component.html',
  styleUrls: ['./card-hero.component.css']
})
export class CardHeroComponent implements OnInit {
  @Input()
  public hero: Hero;
  @Output()
  public heroSelected: EventEmitter<number>;

  constructor(private heroService: HerosService,
              private router: Router) {
    this.heroSelected = new EventEmitter<number>();
  }

  ngOnInit(): void {
  }
  seeHero(){
    this.heroSelected.emit(this.hero.index);
  }

}
