import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HerosService, Hero } from '../../../../services/heros.services';
@Component({
  selector: 'app-search-hero',
  templateUrl: './search-hero.component.html',
  styles: [
  ]
})
export class SearchHeroComponent implements OnInit {
  public Heros: Hero[];
  constructor(private herosService: HerosService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {

   }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.Heros = this.herosService.searchHeros(params.id);
    });
    console.log(this.Heros);
  }
  seeHero(index: number){
    this.router.navigate(['/hero', index]);
  }
}
