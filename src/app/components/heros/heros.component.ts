import { Component, OnInit } from '@angular/core';
import { HerosService, Hero } from '../../services/heros.services';
import { Router } from '@angular/router';


@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styles: [
  ]
})
export class HerosComponent implements OnInit {

  heros: Hero[] = [];
  constructor(private _herosService: HerosService,
              private router: Router){
  }

  ngOnInit(): void {
    this.heros = this._herosService.getHeros();
  }
  seeHero(index: number){
    this.router.navigate(['/hero', index]);
  }
}
