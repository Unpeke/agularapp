import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {HerosService, Hero} from '../../services/heros.services'

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styles: [
  ]
})
export class HeroComponent implements OnInit {
  hero: Hero;
  private srcHomeHero: string;
  constructor(private activatedRoute: ActivatedRoute, private heroService: HerosService, private router: Router) {
    this.activatedRoute.params.subscribe(params => {
      this.hero = heroService.getHero(params.id);
    });
  }
  getUrlHome(): string{
    if (this.hero.casa === 'DC'){
      this.srcHomeHero = 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/DC_Comics_logo.svg/1200px-DC_Comics_logo.svg.png';
    }
    else{
      this.srcHomeHero = 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/MarvelLogo.svg/1200px-MarvelLogo.svg.png';
    }
    return this.srcHomeHero;
  }
  returnToHeros(): void {
    this.router.navigate(['/heros']);
  }
  ngOnInit(): void {
  }

}
